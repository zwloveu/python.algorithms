# bead_sort.py
#!/usr/bin/env python3.9
# -*- coding: utf-8 -*-

"""
经典排序算法 - 珠排序Bead Sort

只能用于正整数
"""
from typing import (
    Tuple, List, Any, 
)

def bead_sort(sequence: List[int]) -> List[int]:
    # 该算法只能用于正整数
    if any(not isinstance(x, int) or x < 0 for x in sequence):
        raise TypeError("该算法只能用于正整数")

    for _ in range(len(sequence)):
        for i, (upper, lower) in enumerate(zip(sequence, sequence[1:])):
            if upper > lower:
                sequence[i] -= upper - lower
                sequence[i+1] += upper - lower
    return sequence


if __name__ == "__main__":
    assert bead_sort([100, 1, 20, 3, 54]) == [1, 3, 20, 54, 100]
